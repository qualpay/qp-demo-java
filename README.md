#README#
The purpose of this sample app is to demonstrate the usage of Qualpay products.

**Dependencies**

You will need

* A qualpay sandbox account. Create your sandbox account at https://api-test.qualpay.com
* Ensure that you have an api security key and the key has access Payment Gateway API and Embedded Fields API. Refer to https://www.qualpay.com/developer/api/testing#security-keys
* Install Play framework and gulp. This application was tested on Play 2.6.11 and gulp 3.9.1
* Chrome or Firefox browser. The application has been tested on latest version of Chrome and Firefox.

**Configuration**

* Update webapp.properties
* Update merchant_id to point to your merchant account id.
* Update security_key to point to your merchant api security_key.
* Update any other property as appropriate for your environment.

**Code samples**

* com.qualpay.app.demo.controller.PaymentGatewayController.java -  Invoke a sale transaction with Qualpay payment gateway.

* app/js/demo/controllers/embedded_controller.js - Use Embedded fields to capture card data and use it to invoke a sale request.

* app/js/demo/controllers/embedded_alacarte_controller.js -  - Use Embedded A La Carte fields to capture card data and use it to invoke a sale request.

* app/js/demo/controllers/demo_controller.js - Use Qualpay Checkout API to generate a checkout link, redirect to Qualpay's checkout page.

**To run the application**

* Make sure that all dependencies are installed

```npm install```

* Start backend

```sbt run```

* Start front end

```gulp watch```

* Access application from browser - http://localhost:9000/

**Qualpay SDKs**

The Platform and Payment Gateway Java SDKs enables you to connect to the Qualpay Platform and Payment Gateway REST APis. The SDKs can be downloaded from the developer portal https://www.qualpay.com/developer/libs/sdk

Following SDK options (Use the "Show SDK options at the bottom of the SDK download page") were used for this application:

Platform SDK
```json
{
  "invokerPackage": "qpPlatform",
  "packageName": "qpPlatform",
  "hideGenerationTimestamp": true,
  "artifactId": "qp-platform-sdk"
}
```

PG SDK
```json
{
  "invokerPackage": "qpPg",
  "packageName": "qpPg",
  "hideGenerationTimestamp": true,
  "artifactId": "qp-pg-sdk"
}
```

To install the library on a local repository, unzip the file and run 

```maven install```

The generated jar libraries are included in the build.sbt file as dependencies.

You can also get a copy of the jar file, by running

```maven package```

The jar files (qp-platform-sdk-1.0.0.jar & qp-pg-sdk-1.0.0.jar) can be found in the target directory.


