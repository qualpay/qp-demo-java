'use strict';

var qpDemoApp = angular.module('qpDemoApp', [
  // Angular
  'ngRoute',
  'ngAnimate',
  'ngSanitize',
  'ngCookies',
  'dictionary',
  'ui.bootstrap'
]);

qpDemoApp.constant('MidResolver', {
  mid :  function(demoAppFactory,  $q){
    return demoAppFactory.getMid($q);
  },
  env: function (demoAppFactory, $q) {
    return demoAppFactory.getEnv($q);
  }
});

  qpDemoApp.config(['$routeProvider', 'MidResolver', function($routeProvider, MidResolver) {
    $routeProvider
    .when('/', {
      redirectTo: '/dashboard'
    })
    .when('/dashboard', {
      title: "Demo",
      templateUrl: 'views/dashboard.html',
    })
    .when('/embedded_fields', {
      title: "Demo - Embedded Checkout",
      templateUrl: 'views/demo/paypage_demo.html',
      controller: 'qpDemoEmbeddedCtrl',
      controllerAs: 'ctrl',
      checkoutType: 'embedded',
      resolve: MidResolver
    })
    .when('/embedded_fields_alacarte', {
      title: "Demo - Embedded Checkout - Individual Fields",
      templateUrl: 'views/demo/paypage_demo.html',
      controller: 'qpDemoEmbeddedAlaCarteCtrl',
      controllerAs: 'ctrl',
      checkoutType: 'embeddedSingle',
      resolve: MidResolver
    }) 
    .when('/embedded_fields/success', {
      title: "Demo - Checkout Embedded Result",
      templateUrl: 'views/result.html',
      controller: 'qpResultCtrl',
      controllerAs: 'ctrl',
      checkoutType: 'embedded',
    })
    .when('/checkout_page', {
      title: "Demo - Checkout Page",
      templateUrl: 'views/demo/paypage_demo.html',
      controller: 'qpDemoCheckoutCtrl',
      controllerAs: 'ctrl',
      checkoutType: 'page',
      resolve: MidResolver
    })
    .when('/test/checkout_page', {
      title: "Testing - Checkout Page",
      templateUrl: 'views/test/checkout_page_test.html',
      controller: 'qpCheckoutTestCtrl',
      controllerAs: 'ctrl',
      resolve: MidResolver
    })
    .when('/test/embedded_fields', {
      title: "Testing - Embedded Fields",
      templateUrl: 'views/test/embedded_test.html',
      controller: 'qpEmbeddedTestCtrl',
      controllerAs: 'ctrl',
      resolve: MidResolver
    })
    .when('/error', {
      title: "Demo - Error",
      templateUrl: 'views/error.html',
      controller: 'qpDemoErrCtrl',
      controllerAs: 'ctrl',
    })
    .otherwise({
      redirectTo: '/error'
    });
  }]);
