

qpDemoApp.controller('qpDemoEmbeddedCtrl', function ($timeout, $location, $route, $log, $scope, $document, $http, $window, $filter, $routeParams, demoAppFactory, dictionary, mid, env) {
  var self = this;
  self.usStates = dictionary.qpStates;

  var onSuccess = function (data) {
    self.payment.cardId = data.card_id;
    self.payment.cardNumber = data.card_number;
    self.payment.error = "";
    $window.qpEmbeddedForm.unloadFrame();
    $http(
      {
        method: 'POST',
        url: '/service/demo/postPayment',
        data: self.payment
      })
      .then(
        function (resp) {
          self.payment.pgId = resp.data.pg_id;
          self.payment.show_receipt = true;
        },
        function (error) {
          self.payment.show_receipt = false;
          $log.debug("Error is ", error.data);
          self.payment.error = "Card declined. Please try again.";
          //Embedded transient key has expired, will have to reload so a new transient key is picked up
          $timeout(self.load, 500);
        }
      );
    $log.debug("Payment is ", self.payment);
    $scope.$apply();
  };

  var onFailure = function (error) {
    $log.debug("Error is ", error);
    self.payment.error = error.msg;
    if (error.detail) {
      for (var key in error.detail) {
        $log.debug(error.detail[key]);
        self.payment.error += '<br>' + error.detail[key];
      }
    }
    $scope.$apply();
  };

  self.payment = {};
  self.payment.billingState = '';
  self.payment.shipping_state = '';
  self.checkoutType = $route.current.$$route.checkoutType;
  self.pgId = $routeParams.pg_id ? $routeParams.pg_id : '';
  self.checkoutId = $routeParams.checkout_id ? $routeParams.checkout_id : '';

  self.lineItems = [
    { 'image': 'images/kitten2.jpg', 'product': 'Kitten', 'option': 'White', 'quantity': 1, 'cost': 45.98 },
    { 'image': 'images/kitten1.jpg', 'product': 'Kitten', 'option': 'Tiger', 'quantity': 2, 'cost': 16.87 },
  ];
  self.total = 0;
  angular.forEach(self.lineItems, function (item) {
    self.total += item.cost * item.quantity;
  });
  self.shipping_cost = 4.99;
  self.amt_tax = 0.0857;
  self.amt_tran = self.total + self.shipping_cost + self.total * self.amt_tax;
  self.cardTypes = dictionary.cardTypes;
  self.payment.show_receipt = false;
  self.payment.purchaseId = 'Acme' + Math.floor((Math.random() * 10000) + 1);
  self.payment.amtTran = self.amt_tran;

  self.load = function () {
    $http(
      {
        method: 'GET',
        url: '/service/demo/getEmbeddedToken'
      }
    ).then(
      function (resp) {
        self.token = resp.data.token;
        $window.qpEmbeddedForm.loadFrame(mid,
          {
            "formId": "demo-payment-form",
            "mode": env,
            "transientKey": self.token,
            "tokenize": true,
            "scrolling": "no",
            "onSuccess": onSuccess,
            "onError": onFailure,
            "achConfig": {
              enabled: true,
              onPaymentTypeChange: function (data) {
                  console.log("Payment type is  ", data.type);
              }
            }
          });
      },
      function (error) {
        $log.debug("Error is ", error);
      }
    );
  };

  self.showPayment = function () {
    self.payment.amtTran = $filter('number')(self.amt_tran, 2);
    self.show_payment = true;
  };

  angular.element(document).ready(function () {
    self.load();
  });

});