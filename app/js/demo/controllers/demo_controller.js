

qpDemoApp.controller('qpDemoCheckoutCtrl', function ($route, $log, $scope, $document, $http, $window, $filter, $routeParams, demoAppFactory, dictionary) {
  var self = this;
  var pref = demoAppFactory.getPreferences();
  self.usStates = dictionary.qpStates;

  var loadTran = function () {
    $http(
      {
        method: 'POST',
        url: '/service/demo/getTranDetails',
        data: { 'checkoutId': self.checkoutId, 'pgId': self.pgId }
      })
      .then(
        function (resp) {
          if (resp.data) {
            self.payment.purchaseId = resp.data.purchase_id;
            self.payment.cardNumber = resp.data.card_number;
            self.payment.amtTran = resp.data.amt_tran;
            self.payment.pgId = resp.data.pg_id;
          }
          self.payment.show_receipt = true;
          $log.debug("Response is ", self.payment);
        },
        function (error) {
          self.payment.show_receipt = false;
          $log.debug("Error is ", error);
        }
      );
  };

  self.payment = {};
  self.payment.billingState = '';
  self.payment.shipping_state = '';
  self.checkoutType = $route.current.$$route.checkoutType;
  console.log("Checkout type is ", self.checkoutType);
  self.pgId = $routeParams.pg_id ? $routeParams.pg_id : '';
  self.checkoutId = $routeParams.checkout_id ? $routeParams.checkout_id : '';

  self.lineItems = [
    { 'image': 'images/kitten2.jpg', 'product': 'Kitten', 'option': 'White', 'quantity': 1, 'cost': 45.98 },
    { 'image': 'images/kitten1.jpg', 'product': 'Kitten', 'option': 'Tiger', 'quantity': 2, 'cost': 16.87 },
  ];
  self.total = 0;
  angular.forEach(self.lineItems, function (item) {
    self.total += item.cost * item.quantity;
  });
  self.shipping_cost = 4.99;
  self.amt_tax = 0.0857;
  self.amt_tran = self.total + self.shipping_cost + self.total * self.amt_tax;
  self.payment.amtTran = self.amt_tran;
  self.cardTypes = dictionary.cardTypes;
  if (self.pgId && self.checkoutId) {
    self.payment.show_receipt = true;
    loadTran();
  }
  else {
    self.payment.show_receipt = false;
  }
  self.payment.purchaseId = 'Acme' + Math.floor((Math.random() * 10000) + 1);

  self.showPayment = function () {
    self.payment.amtTran = $filter('number')(self.amt_tran, 2);
    self.show_payment = true;
  };

  self.submitForm = function () {
    //backend service to get a URL
    self.payment.pref = pref;
    $http({
      method: 'POST',
      url: '/service/demo/getlink',
      data: self.payment
    })
      .then(
        function (resp) {
          $log.debug("Link is ", resp.data);
          var link = resp.data.link;
          var id = resp.data.id;
          $log.debug("Link is ", link);
          self.checkout_id = id;
          if (link && link !== '-')
            $window.location.href = link;
        },
        function (error) {
          self.payment.error = error.data.msg;
          $log.debug("Error is ", error);
        }
      );
  };

});

qpDemoApp.controller('qpResultCtrl', function ($log, $route, $routeParams, $http) {
  var self = this;
  self.checkoutType = $route.current.$$route.checkoutType;
  self.pgId = $routeParams.pg_id ? $routeParams.pg_id : '';
  self.checkoutId = $routeParams.checkout_id ? $routeParams.checkout_id : '';

  var load = function () {
    $http(
      {
        method: 'POST',
        url: '/service/demo/getTranDetails',
        data: { 'checkout_id': self.checkoutId, 'pg_id': self.pgId }
      }
    ).then(
      function (resp) {
        self.tran = resp.data.tran;
      },
      function (error) {
        $log.error("Error is ", error);
      }
    );
  };

  load();
});
