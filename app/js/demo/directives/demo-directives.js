/*
 * Qualpay Header Directive
 * Renders the header for qualpay checkout.
 * The header will take logo from the checkout cache
 * If Header logo is not defined, the header displays the LegalName or DBA name if legal name is not defined
 */
var qpDemoApp = angular.module('qpDemoApp');
qpDemoApp.directive('demoHeader', function() {
  var dir = {
    scope: {},
    controllerAs: 'ctrl',
    templateUrl: 'partials/header.html'
  };

  return dir;
});

qpDemoApp.directive('demoReceipt', function(){
  var dir = {
    restrict: 'E',
    replace: true,
    scope: {},
    controllerAs: 'ctrl',
    templateUrl: 'partials/receipt.html',
    bindToController: {
      tran: '='
    },
  };
  dir.controller = function(){
  };
  return dir;
});
