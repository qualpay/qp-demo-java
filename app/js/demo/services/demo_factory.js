var qpDemoApp = angular.module('qpDemoApp');

qpDemoApp.factory('demoAppFactory',['$log','$location','$http','$q',
  function($log, $location, $http) {
    var self = this;
    var host = $location.protocol() + '://' + $location.host();
    host = $location.port() ? host + ':'  + $location.port() : host;
    var _defaultPreferences = {
      expireInSecs : 300,
      emailReceipt  : false,
      successUrl : host + '/demo/#/checkout_page',
      failureUrl : host + '/demo/#/checkout_page',
      allowPartialPayments : false,
      requestType : 'sale',
    };
    self.preferences = angular.copy(_defaultPreferences);

    return {
      setPreferences: function( preferences){
         self.preferences = angular.copy(preferences);
      },
      getPreferences: function() {
        return self.preferences;
      },
      resetPreferences: function(){
        self.preferences = angular.copy(_defaultPreferences);
      },
      getEnv: function ($q) {
        var deferred = $q.defer();
        if (self.env) return $q.when(self.env);
          $http.get('/service/demo/getEnv').then(
              function(resp){
                if (resp.data){
                  self.env=resp.data.env;
                  deferred.resolve(resp.data.env);
                }
              },
              function(error){
                $log.error("Unable to get Demo Environment", error);
                deferred.reject();
              }
            );
        return deferred.promise;
      },
      getMid: function($q){
        var deferred = $q.defer();
        if (self.mid) return $q.when(self.mid);
          $http.get('/service/demo/getMid').then(
              function(resp){
                if (resp.data){
                  self.mid=resp.data.mid;
                  deferred.resolve(resp.data.mid);
                }
              },
              function(error){
                $log.error("Unable to get Demo Mid", error);
                deferred.reject();
              }
            );
        return deferred.promise;
      },
    };
  }
]);
