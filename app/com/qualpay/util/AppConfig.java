package com.qualpay.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import play.Logger;

public class AppConfig {

  private static AppConfig instance = null;
  private Properties properties;

  protected AppConfig() { }

  public String getString(String key) {
    return properties.getProperty(key);
  }

  /**
   * The full URL to the apiserver, for example https://api.qualpay.com
   * <b>Default: null</b>/
  public String getApiHost() {
    return properties.getProperty("qualpay.api.host", null);
  }

  /**
   * The time it takes to stop waiting for a response from the API server.<br>
   * <b>Default: 10000ms</b>
   * @return
   */
  public int getApiTimeout() {
    try {
      return Integer.parseInt(properties.getProperty("qualpay.demo.timeout"));
    } catch(NumberFormatException e) {
      Logger.error("Invalid property for qualpay.api.timeout: "+properties.getProperty("qualpay.api.timeout"));
      return 10000;
    }
  }

  public String getDemoMid() {
    return properties.getProperty("qualpay.demo.mid");
  }

  public String getDemoEnv() {
    return properties.getProperty("qualpay.demo.env");
  }
  
  public String getApiHost() {
    return properties.getProperty("qualpay.demo.apiHost");
  }

  public String getApiKey() {
    return properties.getProperty("qualpay.demo.apiKey");
  }
  
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder("\n")
    .append("------------------------------------------------------------\n")
    .append("| Qualpay AppConfig webapp.properties                      |\n")
    .append("| Key                                              Value   |\n")
    .append("------------------------------------------------------------\n");
    for(Object o1 : properties.keySet()) {
      String key = (String)o1;
      String val = (String)properties.get(o1);
      sb.append(String.format("%-50s %s", key, val).replace(" ", ".")).append("\n");
    }
    return sb.toString();
  }


  /* Static Methods */

  /** Destroys the cached properties, which will then be reloaded on the next call to {@link AppConfig#getInstance()} */
  public static void invalidate() {
    instance = null;
  }

  /** @return Fetches an instance of the Qualpay Web Application Configuration. */
  public static AppConfig getInstance() {
    if(instance == null) {
      instance = new AppConfig();
      instance.properties = new Properties();

      File f = new File("conf/webapp.properties");

      if(f.exists()) {
        try {
          instance.properties.load(new FileInputStream(f));
        } catch (IOException e) {
          throw new RuntimeException("Error loading conf/webapp.properties", e);
        }
      }
      else
        Logger.error("webapp.properties does not exist!");
    }

    return instance;
  }

}
