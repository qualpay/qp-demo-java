package com.qualpay.util;

import java.util.Map;

import play.mvc.Http.Context;

import com.fasterxml.jackson.databind.JsonNode;


public class PlayHelper {


  public static String getContentType() {
    String ct = "Unknown";
    if(Context.current().request().hasHeader("Content-Type"))
      ct = Context.current().request().getHeader("Content-Type");
    return ct;
  }
  
  public static String[] getHeader(String name){
    Map<String, String[]> headers = Context.current().request().headers();
    return headers.get(name);  
  }
  
  public static JsonNode  getRequestBody(){
    return Context.current().request().body().asJson();
  }
  

  

}
