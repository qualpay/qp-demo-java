package com.qualpay.util;

import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class ApiExceptionParser {

  private static final Logger LOGGER = Logger.getLogger(ApiExceptionParser.class.getName());
  public static String parseApiException(String exceptionMsg){
    JsonParser json = new JsonParser();
    String errors = "";
    try{
      Object obj = json.parse(exceptionMsg);
      if (obj instanceof JsonObject){
        JsonObject exception = (JsonObject)obj;
        JsonElement data = exception.get("data");
        if (data != null && data instanceof JsonObject){
          JsonObject dataJson = data.getAsJsonObject();
          Set<Map.Entry<String, JsonElement>> entries = dataJson.entrySet();
          for (Map.Entry<String, JsonElement> entry: entries) {
            errors += entry.getValue().getAsString() + "," ;
          }

        }         
      }
    } 
    catch (Exception e) {
      LOGGER.log(Level.SEVERE,"Unable to parse Exception message" + e.getMessage());
    }
    return errors;
  }
}
