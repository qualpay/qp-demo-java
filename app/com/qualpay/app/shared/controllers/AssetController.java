package com.qualpay.app.shared.controllers;

import play.mvc.Result;
import play.mvc.Results;

public class AssetController {
    
    public Result redirect(String path) {
      return Results.redirect(path);
    }
}
