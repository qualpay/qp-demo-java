package com.qualpay.app.demo.controllers;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.qualpay.app.demo.models.Payment;
import com.qualpay.util.AppConfig;

import io.swagger.client.api.PaymentGatewayApi;
import io.swagger.client.model.PGApiTransactionRequest;
import io.swagger.client.model.PGApiTransactionResponse;
import play.Logger;
import play.data.Form;
import play.data.FormFactory;
import play.libs.Json;
import play.mvc.Result;
import play.mvc.Results;
import qpPg.ApiClient;
import qpPg.ApiException;

public class PaymentGatewayController {
  @Inject FormFactory formFactory;

  /**
   * Post payment to gateway
   * @return
   */
  public Result postPayment(){
    Form<Payment> pForm = formFactory.form(Payment.class).bindFromRequest();
    //Form<Payment> pForm = Form.form(Payment.class).bindFromRequest();
    Payment paymentForm = pForm.get();
    ObjectNode node = Json.newObject();
    try {
      PaymentGatewayApi api = new PaymentGatewayApi();
      long mid = Long.valueOf(AppConfig.getInstance().getDemoMid()); //Use your Qualpay provided merchant id for the appropriate environment
      String password = AppConfig.getInstance().getApiKey();//Use your security key for the appropriate environment

      System.setProperty("https.protocols", "Tls12");//Qualpay only allows this protocol
      String host = AppConfig.getInstance().getApiHost() + "/pg";

      ApiClient client = new ApiClient();
      client.setBasePath(host);
      client.setPassword(password);
      client.getHttpClient().setReadTimeout(30000, TimeUnit.MILLISECONDS);
      client.getHttpClient().setConnectTimeout(30000, TimeUnit.MILLISECONDS);
      client.getHttpClient().setWriteTimeout(30000, TimeUnit.MILLISECONDS);
      api.setApiClient(client);

      //Authorization sample start
      PGApiTransactionRequest request = new PGApiTransactionRequest();

      // for card not present transactions, including the
      // billing address may help qualify for better rates
      String avsAddress = (null != paymentForm.getBillingAddr1()) ? paymentForm.getBillingAddr1().trim() : "";
      avsAddress=(avsAddress.length() > 20) ? avsAddress.substring(0,20):avsAddress;

      String avsZip = (null != paymentForm.getBillingZip()) ? paymentForm.getBillingZip().trim() : "";
      avsZip = (avsZip.length() > 9) ? avsZip.substring(0,9):avsZip;

      String cardHolderName = (null != paymentForm.getBillingFirstName()) ? paymentForm.getBillingFirstName() : "";
      cardHolderName += (null != paymentForm.getBillingLastName()) ? paymentForm.getBillingLastName() : "";
      cardHolderName = (cardHolderName.length() > 64) ? cardHolderName.substring(0, 64) : cardHolderName;
      
      // depending on the card type, providing zip code
      // and invoice number(purchase id) may help qualify for better rates
      request.merchantId(mid)
      .amtTran(paymentForm.getAmtTran())
      .avsAddress(avsAddress)
      .avsZip(avsZip)
      .merchRefNum(paymentForm.getMerchantRefNum())    //Optional reference number 
      .purchaseId(paymentForm.getPurchaseId())
      .cardholderName(cardHolderName)
      //.tokenize(true)                                  //Optional if this is a single use card_id and you would like to make it permanent for future requests.
      .cardId( paymentForm.getCardId());


      
      //Optional add extra information to report data object
      ObjectNode customer = Json.newObject();
      customer.put("customer_first_name", paymentForm.getBillingFirstName());
      customer.put("customer_last_name", paymentForm.getBillingLastName());
      customer.put("customer_email", paymentForm.getCustomerEmail());
      customer.put("customer_phone", paymentForm.getCustomerPhone());
      customer.put("billing_street_addr1", paymentForm.getBillingAddr1());
      customer.put("billing_city", paymentForm.getBillingCity());
      customer.put("billing_state", paymentForm.getBillingState());
      customer.put("billing_zip", paymentForm.getBillingZip());
      
      ArrayNode reportArr = Json.newArray();
      if (null != reportArr) {
        reportArr.add(customer);
      }
      request.reportData(reportArr.asText());

    
      PGApiTransactionResponse result = api.sale(request);
      Logger.debug("Success - " + result.getRmsg());
      node.put("pg_id", result.getPgId());
      node.put("RCode",result.getRcode());
      node.put("purchase_id", paymentForm.getPurchaseId());
      node.put("amt_tran", paymentForm.getAmtTran());
      node.put("tran_currency", paymentForm.getTranCurrency());
      node.put("tran_time", new Date().toString());
      Logger.debug("Transaction is " + node.toString());
    } 
    catch (ApiException e) {
      Logger.error("Error processing payment" , e);
      Logger.error(e.getResponseBody());
      ObjectNode result = Json.newObject();
      JsonParser json = new JsonParser();
      Object obj = json.parse(e.getResponseBody());
      if (obj instanceof JsonObject){
        JsonObject exception = (JsonObject)obj;
        result.put("rCode",  exception.get("rcode").getAsString());
        result.put("msg",  exception.get("rmsg").getAsString());    
        result.put("pg_id", exception.get("pg_id").getAsString());
      }
      return Results.badRequest(result);
    }
    catch (Exception e) {
      Logger.error("Error processing payment" , e);
      ObjectNode result = Json.newObject();
      result.put("code", 99);
      result.put("msg", "Internal Error");
      return Results.internalServerError(result);
    }
    return Results.ok(node);
  }
}
