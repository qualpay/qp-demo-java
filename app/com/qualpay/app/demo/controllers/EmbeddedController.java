package com.qualpay.app.demo.controllers;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.qualpay.util.AppConfig;

import io.swagger.client.api.EmbeddedFieldsApi;
import play.data.FormFactory;
import play.libs.Json;
import play.mvc.Result;
import play.mvc.Results;
import qpPlatform.ApiException;


public class EmbeddedController {
  @Inject FormFactory formFactory;
  
  public static qpPlatform.ApiClient client = new qpPlatform.ApiClient();

  static{
    client.setPassword(AppConfig.getInstance().getApiKey());
    client.setBasePath(AppConfig.getInstance().getApiHost() + "/platform");
    client.getHttpClient().setReadTimeout(30000, TimeUnit.MILLISECONDS);
    client.getHttpClient().setConnectTimeout(30000, TimeUnit.MILLISECONDS);
    client.getHttpClient().setWriteTimeout(30000, TimeUnit.MILLISECONDS);
  }
  
  /**
   * Get Transient Token from embedded fields
   * @return
   */
  public Result getEmbeddedToken(){
    ObjectNode node = Json.newObject();
    String token;
    try {
      token = new EmbeddedFieldsApi(client).getEmbeddedTransientKey().getData().getTransientKey();
      node.put("token", token);
    } catch (ApiException e) {
      e.printStackTrace();
    }  
    return Results.ok(node);
  }

}
