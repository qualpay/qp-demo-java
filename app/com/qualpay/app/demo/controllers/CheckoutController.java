package com.qualpay.app.demo.controllers;

import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.qualpay.app.demo.models.CheckoutPayment;
import com.qualpay.app.demo.models.DemoPreferences;
import com.qualpay.app.demo.models.Payment;
import com.qualpay.util.ApiExceptionParser;
import com.qualpay.util.AppConfig;

import io.swagger.client.api.QualpayCheckoutApi;
import io.swagger.client.model.Checkout;
import io.swagger.client.model.CheckoutLinkResponse;
import io.swagger.client.model.CheckoutPreferences;
import io.swagger.client.model.CheckoutPreferences.RequestTypeEnum;
import io.swagger.client.model.CheckoutRequest;
import io.swagger.client.model.CheckoutResponse;
import io.swagger.client.model.GatewayResponse;
import play.Logger;
import play.data.Form;
import play.data.FormFactory;
import play.libs.Json;
import play.mvc.Result;
import play.mvc.Results;
import qpPlatform.ApiException;


public class CheckoutController {
  @Inject FormFactory formFactory;
  
  public static qpPlatform.ApiClient client = new qpPlatform.ApiClient();

  static{
    client.setPassword(AppConfig.getInstance().getApiKey());
    client.setBasePath(AppConfig.getInstance().getApiHost() + "/platform");
    client.getHttpClient().setReadTimeout(30000, TimeUnit.MILLISECONDS);
    client.getHttpClient().setConnectTimeout(30000, TimeUnit.MILLISECONDS);
    client.getHttpClient().setWriteTimeout(30000, TimeUnit.MILLISECONDS);

  }
  
  /**
   * Create a Checkout Link for a transaction
   */
  public Result getCheckoutLink() {       
    Form<Payment> pForm = formFactory.form(Payment.class).bindFromRequest();
    Result ret=null;
      
    
    Logger.debug("Form is " +  pForm);
    try{
      CheckoutLinkResponse response = getCheckoutLink(pForm.get());

      ObjectNode result = Json.newObject();
      if ( response != null && response.getCode() == 0 ) {
      result.put("link", response.getData().getCheckoutLink());
      result.put("id", response.getData().getCheckoutId());
      ret =  Results.ok(result);
     //Additional processing here        
      }
    } catch (Exception ex){
      ObjectNode result = Json.newObject();
      ex.printStackTrace();
      if (ex instanceof ApiException){   
        ApiException resp = (ApiException)ex;
        String errors = ApiExceptionParser.parseApiException(resp.getResponseBody());
        result.put("code",  2);
        result.put("msg", errors);
      }else {
        result.put("code", 99);
        result.put("msg", "Internal Error");
      }
      ret = Results.badRequest(result);
    }
    return ret;
  }
  
  /**
   * Get Details of a transaction for a Checkout
   * @return
   */
  public Result getTransactionDetails(){
    Form<CheckoutPayment> pForm = formFactory.form(CheckoutPayment.class).bindFromRequest();
    Result ret;
    
    CheckoutPayment paymentForm = pForm.get();
    
    ObjectNode tran = getTransactionDetails( paymentForm.getCheckoutId(), paymentForm.getPgId());
    
    if (tran != null )
      ret =  Results.ok(tran);
    else
      ret= Results.badRequest();
    return ret;
  }
  
  
  
  /**
   * Gets the demo mid from config file
   * @return
   */
  public Result getDemoMid(){
    ObjectNode node = Json.newObject();
    node.put("mid", AppConfig.getInstance().getDemoMid());
    return Results.ok(node);
  }
  
  /**
   * Gets the demo mid from config file
   * @return
   */
  public Result getDemoEnv(){
    ObjectNode node = Json.newObject();
    node.put("env", AppConfig.getInstance().getDemoEnv());
    return Results.ok(node);
  }
  
 
  private ObjectNode getTransactionDetails( String checkoutId, String gatewayId){
    ObjectNode result = Json.newObject();
    try{    
      QualpayCheckoutApi api = new QualpayCheckoutApi(client);
      CheckoutResponse response = api.getDetails(checkoutId);
      Logger.debug("Get Checkout details for " + checkoutId + response);
      if ( response.getCode() == 0 ) {
        Checkout checkout = response.getData();
        checkoutId = checkout.getCheckoutId();
        //Query individual transactions
        List<GatewayResponse> trans = checkout.getTransactions();
        for (GatewayResponse tran: trans){
          String pgId = tran.getPgId();
          if (pgId != null && pgId.trim().equals(gatewayId.trim())){
            result.put("pg_id", tran.getPgId());
            result.put("card_number", tran.getCardNumber());
            result.put("purchase_id", tran.getPurchaseId());
            result.put("amt_tran", tran.getAmtTran());
            result.put("tran_currency", tran.getTranCurrency());
            result.put("tran_time", tran.getTranTime());
            break;
          }
        }     
      } else{
        Logger.error("Error reading checkout Resource Code:" +     response.getCode() + ", " + "Message: " + response.getMessage());
      }
    } 
    catch (Exception ex) {
      Logger.error("Exception getting checkout transaction details" + ex);
      result = null;
    }
    return result;
    
  }
  
  private CheckoutLinkResponse getCheckoutLink(Payment form) throws Exception{
    CheckoutLinkResponse response = null;
    try{
      
      QualpayCheckoutApi api = new QualpayCheckoutApi(client);
      
      CheckoutRequest request = new CheckoutRequest();

     
     DemoPreferences preferences = form.getPref();
     RequestTypeEnum reqType = null;
     switch (preferences.getRequestType()){
     case "sale" : reqType = RequestTypeEnum.SALE; break;
     case "auth" : reqType = RequestTypeEnum.AUTH; break;
     }
     CheckoutPreferences pref = new CheckoutPreferences();
     pref.expireInSecs(preferences.getExpireInSecs())
                             .allowPartialPayments(preferences.isAllowPartialPayments())
                             .emailReceipt(preferences.emailReceipt)
                             .successUrl(preferences.getSuccessUrl())
                             .failureUrl(preferences.getFailureUrl());
                             //.allowSaveCard(preferences.isAllowSaveCard());
     if (reqType != null){
       pref.requestType(reqType);
     }

      request.amtTran(form.getAmtTran())
             //.preferences(pref)
             .tranCurrency(form.getTranCurrency())
             .purchaseId(form.getPurchaseId())
             .merchRefNum(form.getMerchantRefNum())
             .customerFirstName(form.getBillingFirstName())
             .customerLastName(form.getBillingLastName())
             .customerId(form.getCustomerId())
             .billingAddr1(form.getBillingAddr1())
             .billingCity(form.getBillingCity())
             .billingState(form.getBillingState())
             .billingZip(form.getBillingZip())
             .customerEmail(form.getCustomerEmail())
             .customerPhone(form.getCustomerPhone())
             .profileId(form.getProfileId());


      response = api.addCheckout(request);
    } 
    catch (Exception ex) {
      throw ex;
    }

    return response;
  }
}
