package com.qualpay.app.demo.controllers;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.qualpay.util.AppConfig;

import play.data.FormFactory;
import play.libs.Json;
import play.mvc.Result;
import play.mvc.Results;


public class DemoController {
  @Inject FormFactory formFactory;
  
  public static qpPlatform.ApiClient client = new qpPlatform.ApiClient();

  static{
    client.setPassword(AppConfig.getInstance().getApiKey());
    client.setBasePath(AppConfig.getInstance().getApiHost() + "/platform");
    client.getHttpClient().setReadTimeout(30000, TimeUnit.MILLISECONDS);
    client.getHttpClient().setConnectTimeout(30000, TimeUnit.MILLISECONDS);
    client.getHttpClient().setWriteTimeout(30000, TimeUnit.MILLISECONDS);
  }
  
 
  /**
   * Gets the demo mid from config file
   * @return
   */
  public Result getDemoMid(){
    ObjectNode node = Json.newObject();
    node.put("mid", AppConfig.getInstance().getDemoMid());
    return Results.ok(node);
  }
  
  /**
   * Gets the demo mid from config file
   * @return
   */
  public Result getDemoEnv(){
    ObjectNode node = Json.newObject();
    node.put("env", AppConfig.getInstance().getDemoEnv());
    return Results.ok(node);
  }
  
}
