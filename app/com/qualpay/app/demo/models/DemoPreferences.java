package com.qualpay.app.demo.models;

public class DemoPreferences {
  public String successUrl;
  public String failureUrl;
  public String notificationUrl;
  public int expireInSecs;
  public boolean allowPartialPayments;
  public boolean emailReceipt;
  public String requestType;
  public boolean allowSaveCard;
  
  public String getSuccessUrl() {
    return successUrl;
  }
  public void setSuccessUrl(String successUrl) {
    this.successUrl = successUrl;
  }
  public String getFailureUrl() {
    return failureUrl;
  }
  public void setFailureUrl(String failureUrl) {
    this.failureUrl = failureUrl;
  }
  public String getNotificationUrl() {
    return notificationUrl;
  }
  public void setNotificationUrl(String notificationUrl) {
    this.notificationUrl = notificationUrl;
  }
  public int getExpireInSecs() {
    return expireInSecs;
  }
  public void setExpireInSecs(int expInSec) {
    this.expireInSecs = expInSec;
  }
  public boolean isAllowPartialPayments() {
    return allowPartialPayments;
  }
  public void setAllowPartialPayments(boolean allowPartialPayments) {
    this.allowPartialPayments = allowPartialPayments;
  }
  public boolean isEmailReceipt() {
    return emailReceipt;
  }
  public void setEmailReceipt(boolean emailReceipt) {
    this.emailReceipt = emailReceipt;
  }
  public String getRequestType() {
    return requestType;
  }
  public void setRequestType(String requestType) {
    this.requestType = requestType;
  }
  public boolean isAllowSaveCard() {
    return allowSaveCard;
  }
  public void setAllowSaveCard(boolean allowSaveCard) {
    this.allowSaveCard = allowSaveCard;
  }
  @Override
  public String toString() {
    return "DemoPreferences [successUrl=" + successUrl + ", failureUrl="
        + failureUrl + ", notificationUrl=" + notificationUrl
        + ", expireInSecs=" + expireInSecs + ", allowPartialPayments="
        + allowPartialPayments + ", emailReceipt=" + emailReceipt
        + ", requestType=" + requestType + ", allowSaveCard=" + allowSaveCard
        + "]";
  }
  

}
