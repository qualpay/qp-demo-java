package com.qualpay.app.demo.models;

public class DemoTran {
  public String pgId;
  public String checkoutId;
  public float amtTran;
  public String currency;
  public String purchaseId;
  public String timestamp;
  public String getPgId() {
    return pgId;
  }
  public void setPgId(String pgId) {
    this.pgId = pgId;
  }
  public String getCheckoutId() {
    return checkoutId;
  }
  public void setCheckoutId(String checkoutId) {
    this.checkoutId = checkoutId;
  }
  public float getAmtTran() {
    return amtTran;
  }
  public void setAmtTran(float amtTran) {
    this.amtTran = amtTran;
  }
  public String getCurrency() {
    return currency;
  }
  public void setCurrency(String currency) {
    this.currency = currency;
  }
  public String getPurchaseId() {
    return purchaseId;
  }
  public void setPurchaseId(String purchaseId) {
    this.purchaseId = purchaseId;
  }
  public String getTimestamp() {
    return timestamp;
  }
  public void setTimestamp(String timestamp) {
    this.timestamp = timestamp;
  }
  
  @Override
  public String toString() {
    return "Transaction [pgId=" + pgId + ", checkoutId=" + checkoutId
        + ", amtTran=" + amtTran + ", currency=" + currency + ", purchaseId="
        + purchaseId + ", timestamp=" + timestamp + "]";
  }
  
}
