package com.qualpay.app.demo.models;



public class Payment {
  
  public double amtTran;
  public String tranCurrency;
  public String cardId;
  public String profileId;
  
  public String customerId;
  
  
  //Billing fields
	public  String billingFirstName;
	public  String billingLastName;
	public  String customerEmail;
	public  String customerPhone;
	public  String billingAddr1;
	public  String billingCity;
	public  String billingState;
	public  String billingZip;
	public  String billingCountry;
	
	//Invoicing
  public String purchaseId;
  public String merchantRefNum;
	
	public DemoPreferences pref;

  public String getCustomerEmail() {
    return customerEmail;
  }
  public void setCustomerEmail(String customerEmail) {
    this.customerEmail = customerEmail;
  }
  public String getCustomerPhone() {
    return customerPhone;
  }
  public void setCustomerPhone(String customerPhone) {
    this.customerPhone = customerPhone;
  }
  public String getCardId() {
    return cardId;
  }
  public void setCardId(String cardId) {
    this.cardId = cardId;
  }

  public double getAmtTran() {
    return amtTran;
  }
  public void setAmtTran(double amtTran) {
    this.amtTran = amtTran;
  }
  public String getTranCurrency() {
    return tranCurrency;
  }
  public void setTranCurrency(String tranCurrency) {
    this.tranCurrency = tranCurrency;
  }
  public String getPurchaseId() {
    return purchaseId;
  }
  public void setPurchaseId(String purchaseId) {
    this.purchaseId = purchaseId;
  }
  public String getMerchantRefNum() {
    return merchantRefNum;
  }
  public void setMerchantRefNum(String merchantRefNum) {
    this.merchantRefNum = merchantRefNum;
  }
  public String getBillingFirstName() {
    return billingFirstName;
  }
  public void setBillingFirstName(String billingFirstName) {
    this.billingFirstName = billingFirstName;
  }
  public String getBillingLastName() {
    return billingLastName;
  }
  public void setBillingLastName(String billingLastName) {
    this.billingLastName = billingLastName;
  }
  public String getBillingAddr1() {
    return billingAddr1;
  }
  public void setBillingAddr1(String billingAddr1) {
    this.billingAddr1 = billingAddr1;
  }
  public String getBillingCity() {
    return billingCity;
  }
  public void setBillingCity(String billingCity) {
    this.billingCity = billingCity;
  }
  public String getBillingState() {
    return billingState;
  }
  public void setBillingState(String billingState) {
    this.billingState = billingState;
  }
  public String getBillingZip() {
    return billingZip;
  }
  public void setBillingZip(String billingZip) {
    this.billingZip = billingZip;
  }
  public String getBillingCountry() {
    return billingCountry;
  }
  public void setBillingCountry(String billingCountry) {
    this.billingCountry = billingCountry;
  }
  public DemoPreferences getPref() {
    return pref;
  }
  public void setPref(DemoPreferences pref) {
    this.pref = pref;
  }
  
  public String getProfileId() {
    return profileId;
  }
  public void setProfileId(String profileId) {
    this.profileId = profileId;
  }
  public String getCustomerId() {
    return customerId;
  }
  public void setCustomerId(String customerId) {
    this.customerId = customerId;
  }
  @Override
  public String toString() {
    return "Payment [amtTran=" + amtTran + ", tranCurrency=" + tranCurrency
        + ", cardId=" + cardId + ", profileId=" + profileId + ", customerId="
        + customerId + ", billingFirstName=" + billingFirstName
        + ", billingLastName=" + billingLastName + ", customerEmail="
        + customerEmail + ", customerPhone=" + customerPhone
        + ", billingAddr1=" + billingAddr1 + ", billingCity=" + billingCity
        + ", billingState=" + billingState + ", billingZip=" + billingZip
        + ", billingCountry=" + billingCountry + ", purchaseId=" + purchaseId
        + ", merchantRefNum=" + merchantRefNum + ", pref=" + pref + "]";
  }
	
}
