package com.qualpay.app.demo.models;

public class CheckoutPayment {
  public String pgId;
  public String checkoutId;
 
  public String getPgId() {
    return pgId;
  }
  public void setPgId(String pgId) {
    this.pgId = pgId;
  }
  public String getCheckoutId() {
    return checkoutId;
  }
  public void setCheckoutId(String id) {
    this.checkoutId = id;
  }

}
