var gulp = require('gulp'),
debug = require('gulp-debug'),
rev = require('gulp-rev'),
gulpif = require('gulp-if'),
seq = require('run-sequence'),
cached = require('gulp-cached'),
remember = require('gulp-remember'),
sass = require('gulp-ruby-sass'),
autoprefixer = require('gulp-autoprefixer'),
cssnano = require('gulp-cssnano'),
jshint = require('gulp-jshint'),
uglify = require('gulp-uglify'),
// imagemin = require('gulp-imagemin'),
rename = require('gulp-rename'),
concat = require('gulp-concat'),
// notify = require('gulp-notify'),
// cache = require('gulp-cache'),
// livereload = require('gulp-livereload'),
ngModuleFilter = require("gulp-ng-module-filter"),
ngGraph = require('gulp-ng-graph'),
del = require('del'),
gutil = require('gulp-util'),
inject = require('gulp-inject');

// Include the config module.
var qualpay = require('./conf/build/appconfig.js');

/*
 * Generic functions
 */
var write = function(str) {
  process.stdout.write(str);
};

var writeObj = function(obj) {
  process.stdout.write(JSON.stringify(obj, null, 2));
};

/**
 * Dynamically builds a task to build an application's JS app.
 * @param  {object}   app       See var Qualpay.
 * @return {object}             Gulp.js task.
 */
var buildJS = function(app) {
  gulp.task('$build'+app.label+'JS', function() {
    return gulp.src(app.js.src)
    // .pipe(debug())                                             // To see all files being fed into ngModuleFilter.
    .pipe(ngModuleFilter({appModule: app.appModule}))
    // .pipe(debug())                                             // To see what ngModuleFilter has filtered out.
    .pipe(cached(app.label+'Scripts'))                            // Run the results through the cache.
    // .pipe(debug())                                             // See what the cache says has changed.
    .pipe(gulpif(!gutil.env.noLint, jshint('.jshintrc')))         // The .jshintignore will filter out our vendor files, etc.
    .pipe(gulpif(!gutil.env.noLint, jshint.reporter('default')))
    .pipe(remember(app.label+'Scripts'))                          // Remember pulls all the files going into the cache, not just what the cache spit out.
    // .pipe(debug())                                             // See what the cache says has changed.
    .pipe(gulpif(!gutil.env.noConcat, concat('app.js')))
    .pipe(gulpif(gutil.env.minify, rename({suffix: '.min'})))
    .pipe(gulpif(gutil.env.minify, uglify({mangle: false})))
    .pipe(gulpif(gutil.env.revision, rev()))
    .pipe(gulp.dest(function(file) {
      if(gutil.env.noConcat) {  return app.managed + file.base; } // When not concating, include the full path as a way to insure include order.
      else {                    return app.managed; }
    }));
  });
};

/**
 * Dynamically builds a task to build an application's CSS.
 * @param  {object}   app See var Qualpay.
 * @return {object}       Gulp.js stream.
 */
var buildCSS = function(app) {
  gulp.task('$build'+app.label+'CSS', function() {
    return sass(app.css.sass, { style: 'expanded' })
    .pipe(cached(app.label+'Styles'))                 // Run the results through the cache.
    // .pipe(debug())                                 // See what the cache says has changed.
    .pipe(autoprefixer('last 2 version'))             // Adds all relevant vendor-specific CSS prefixes
    .pipe(remember(app.label+'Styles'))               // Remember pulls all the files going into the cache, not just what the cache spit out.
    .pipe(gulpif(gutil.env.minify && !app.css.neverMinify, rename({suffix: '.min'})))
    .pipe(gulpif(gutil.env.minify && !app.css.neverMinify, cssnano({zindex:false})))
    .pipe(gulpif(gutil.env.revision && !app.css.neverRevision, rev()))
    .pipe(gulp.dest(app.managed));
  });
};

var buildAngular = function(app) {
  gulp.task('$build'+app.label+'Angular', function() {
    return gulp.src(app.js.angular)
    .pipe(concat('angular.js'))
    .pipe(gulpif(gutil.env.revision, rev()))
    .pipe(gulp.dest(app.managed));
  });
};

var injectJS = function(app) {
  // Dependency on build(APP)JS
  gulp.task('$inject'+app.label+'JS', ['$build'+app.label+'JS'], function() {
    return gulp.src(app.public + '/index.html')
    .pipe(inject(gulp.src(app.managed + '**/*.js', {read: false}), {relative: true}))
    .pipe(gulp.dest(app.public));
  });
};

var injectCSS = function(app) {
  // Dependency on build(APP)CSS
  gulp.task('$inject'+app.label+'CSS', ['$build'+app.label+'CSS'], function() {
    return gulp.src(app.public + '/index.html')
    .pipe(inject(gulp.src(app.managed + '**/*.css', {read: false}), {relative: true}))
    .pipe(gulp.dest(app.public));
  });
};

var injectALL = function(app) {
  // Dependency on build(APP)CSS & JS
  gulp.task('$inject'+app.label, ['$build'+app.label+'JS', '$build'+app.label+'CSS'], function() {
    return gulp.src(app.public + '/index.html')
    .pipe(inject(gulp.src([app.managed + '**/*.js', app.managed + '**/*.css'], {read: false}), {relative: true}))
    .pipe(gulp.dest(app.public));
  });
};

var clean = function(app) {
  gulp.task('$clean'+app.label, function() {
    return del(app.managed);
  });
};



/*
 * Run through each element in the var qualpay, and dynamically build tasks for them.
 */
process.stdout.write('Creating tasks for '+qualpay.apps.length+' webapps...');
var buildTasks = [];
var cleanTasks = [];
for (var i=0; i < qualpay.apps.length; i++) {
  process.stdout.write(' ' + qualpay.apps[i].label);
  qualpay.apps[i].managed = qualpay.apps[i].public + 'managed/';
  buildAngular(qualpay.apps[i]);
  buildJS(qualpay.apps[i]);
  buildCSS(qualpay.apps[i]);
  injectJS(qualpay.apps[i]);
  injectCSS(qualpay.apps[i]);
  injectALL(qualpay.apps[i]);
  clean(qualpay.apps[i]);
  buildTasks.push('build' + qualpay.apps[i].label); // Non-dynamic task (defined below).
  cleanTasks.push('$clean' + qualpay.apps[i].label); // Dynamic task.
}
process.stdout.write(' (Done)\n');

/** Build All */
gulp.task('build', ['help', 'buildHosted'], function() {
  return seq(buildTasks);
});

/** Clean All */
gulp.task('clean', function() {
  return seq(cleanTasks);
});

gulp.task('buildDemo', ['help', '$buildDemoAngular', '$buildDemoCSS', '$buildDemoJS', '$injectDemo']);

gulp.task('buildHosted', ['help', '$buildHostedCSS']);

/**
 *  requires: $brew install graphviz
 *  Generate graph image with: dot -Tpng -o graph/module.png graph/ng-graph.dot
 */
gulp.task('graph', ['help'], function() {
  del('graph');
  return gulp.src(['app/js/demoApp.js', 'app/js/shared/**/*.js', 'app/js/demo/**/*.js'])
        .pipe(ngGraph())
        .pipe(gulp.dest('./graph/'));
});

gulp.task('watch', ['build', 'help'], function() {
  if(gutil.env.revision) {
    write('\n!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n');
    write('The revision flag is enabled. When watching you probably don\'t want\n');
    write('that, as new file versions are generated and injected each time you save.\n');
    write('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n\n');
  }

  // Watch .scss files
  for(var h=0; h<qualpay.apps.length; ++h) {
    if(qualpay.apps[h].hasOwnProperty('css') && qualpay.apps[h].css.hasOwnProperty('src')) {
      write('Watching '+qualpay.apps[h].label+' CSS '+qualpay.apps[h].css.src+'\n');
      gulp.watch(qualpay.apps[h].css.src, ['$build'+qualpay.apps[h].label+'CSS']);
    }
  }

  // Watch .js files
  for(var i=0; i<qualpay.apps.length; ++i) {
    if(qualpay.apps[i].hasOwnProperty('js') && qualpay.apps[i].js.hasOwnProperty('src')) {
      write('Watching '+qualpay.apps[i].label+' JS '+qualpay.apps[i].js.src+'\n');
      gulp.watch(qualpay.apps[i].js.src, ['$build'+qualpay.apps[i].label+'JS']);
    }
  }
});

gulp.task('help', function() {
  write(qualpay.logo);

  write('Use gulp --tasks to see a list of Qualpay tasks.\n');
  write('Tasks beginning with $ are programatically generated for each application.\n');
  write('\n');
  write('Options for the qp-web front end:\n');
  write('  --minify      Minifies the output JS and CSS\n');
  write('  --revision    Adds a version hash to the output app JS and CSS.\n');
  write('  --noConcat    Keeps all application files individual, and does not\n');
  write('                concatinate them all into one app.js file.\n');
  write('  --noLint      Skips JS linting.\n');
  write('\n');
});

gulp.task('default', ['help']);
