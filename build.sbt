name := """qp-demo"""

version := "dist"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.12.4"

resolvers += "Local Maven Repository" at "file://"+Path.userHome.absolutePath+"/.m2/repository"

libraryDependencies ++= Seq(
  "org.json"%"json"%"20140107",
  "com.warrenstrange"%"googleauth"%"0.4.3",
  "net.sf.uadetector"%"uadetector-resources"%"2014.10"
)

libraryDependencies ++= Seq(
        javaJdbc,
        "mysql" % "mysql-connector-java" % "5.1.36"
)

// Make java SDK changing to always load. (Move up to other dependencies once it's properly versioned)
libraryDependencies += "io.swagger"%"qp-platform-sdk"%"1.0.0" changing()

// Swagger client dependency
libraryDependencies += "io.swagger"%"qp-pg-sdk"%"1.0.0" changing()

libraryDependencies += "javax.xml.bind" % "jaxb-api" % "2.1"

libraryDependencies += guice

// Use Injected Routes (play 2.4)
routesGenerator := InjectedRoutesGenerator
