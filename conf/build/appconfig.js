/**
 * Qualpay Web App
 * Front end build config.
 */
var settings = {

  logo: '   ____              __\n'+
        '  / __ \\__  ______ _/ /___  ____ ___  __\n'+
        ' / / / / / / / __ `/ / __ \\/ __ `/ / / /\n'+
        '/ /_/ / /_/ / /_/ / / /_/ / /_/ / /_/ / \n'+
        '\\___\\_\\__,_/\\__,_/_/ .___/\\__,_/\\__, /\n'+
        '                  /_/         /_____/\n',
  
  // An array of all angular applications
  apps: [
       {
        "label": "Demo",
        "appModule": "qpDemoApp",
        "public": "public/demo/",
        "js": {
          "angular": [
            "app/js/angular/1.4.3/angular.min.js",
            "app/js/angular/1.4.3/angular-animate.min.js",
            "app/js/angular/1.4.3/angular-route.min.js",
            "app/js/angular/1.4.3/angular-sanitize.min.js",
            "app/js/angular/1.4.3/angular-cookies.min.js",
          ],
          "src": [
            "app/js/vendor/**/*.js",
            "app/js/shared/**/*.js",
            "app/js/demoApp.js",
            "app/js/demo/**/*.js"
          ]
        },
        "css": {
          "src": [
            "app/css/demo/**/*.scss",
            "app/css/shared/**/*.scss"
          ],
          "sass": "app/css/demo/app.scss"
        }
      },
      {
        "label": "Hosted",
        "public": "public/hosted/",
        "css": {
          "neverMinify": true,
          "neverRevision": true,
          "src": [
            "app/css/hosted/**/*.scss",
            "app/css/shared/**/*.scss"
          ],
          "sass": "app/css/hosted/app.scss"
        }
      },
  ]
};

module.exports = settings;
